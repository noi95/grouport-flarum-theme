# Grouport Theme

![License](https://img.shields.io/badge/license-MIT-blue.svg) [![Latest Stable Version](https://img.shields.io/packagist/v/noi-fmc/grouport-theme.svg)](https://packagist.org/packages/noi-fmc/grouport-theme)

A [Flarum](http://flarum.org) extension. Grouport flarum theme

### Installation

Use [Bazaar](https://discuss.flarum.org/d/5151-flagrow-bazaar-the-extension-marketplace) or install manually with composer:

```sh
composer require noi-fmc/grouport-theme
```

### Updating

```sh
composer update noi-fmc/grouport-theme
```

### Links

- [Packagist](https://packagist.org/packages/noi-fmc/grouport-theme)
